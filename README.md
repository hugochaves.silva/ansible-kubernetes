# Install Kubernetes with ansible

Use this ansible when you want to install your k8s cluster.
This was tested with kubernetes version with `1.20.1-00`.

## Variables that we can change

You can go to `vars folder` and you will find everyhing that can be configured in this installation.
In order to configure your control plane and nodes you have the file `nodes.yml`.

## Usage

First step is preparing all your nodes and control plane. This preparation require an ssh connection.

After configuring the `hosts.yml` file we are now ready to start the provisioning.

First the start with installing/configuring the pre-requirements on each host.

1. Install all the community dependencies
    1. `ansible-galaxy collection install -r collections/requirements.yml`
1. Add user on all hosts and configure hosts file reading our `hosts.yml`.
    1. `ansible-playbook -i hosts.yml pre-requirements.yml`
1. Install k8s on all hosts.
    1. `ansible-playbook -i hosts.yml install-k8s.yml`
1. Install control planes, running this command multiple times, will re-install the control plames.
    1. `ansible-playbook -i hosts.yml control-planes.yml`
1. Add nodes to control plane, you can add more nodes to hosts.yml and run this command again to add them to control plane.
    1. `ansible-playbook -i hosts.yml nodes.yml`
    1. In case you want to re-install the nodes use the following command.
        1. `ansible-playbook -i hosts.yml nodes.yml -e "FORCE_INSTALL=true"`
1. In case you want to install helm.
    1. `ansible-playbook -i hosts.yml install-helm.yml`
